# SplitWick Frontend (works locally for now)
SplitWick is a web application which allows people to split promotions and get products cheaper.

# A diagram of website's architecture.
![SplitwickDiagram](/uploads/39689f488bc9eb400232613b0be99edf/SplitwickDiagram.png)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development (this runs server for now)
```
yarn serve
```


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
