import axios from 'axios';

const API_URL = process.env.VUE_APP_BASE_URL

class AuthService {
    login(user) {
        return axios.post(
            API_URL + '/auth/jwt/create/', {
            username: user.username,
            password: user.password
        }
        ).then(response => {
            // console.log(user)
            if (response.data.access) {
                var data = Object.assign({'username': user.username}, response.data);
                localStorage.setItem('user', JSON.stringify(data));
            }
            return data
        })

    }
    logout() {
        localStorage.removeItem('user');
        localStorage.removeItem('profile');
    }

    register(user) {
        // console.log(user)
        
        var resp = axios.post(API_URL + '/auth/users/', {
            username: user.username,
            email: user.email,
            password: user.password
        });
        console.log(resp)
        return resp
    }
}
export default new AuthService();