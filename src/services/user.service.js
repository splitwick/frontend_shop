import axios from 'axios';
import authHeader from './auth-header'

const ME = process.env.VUE_APP_BASE_URL +'/auth/users/me';
const ALL_PROFILES = process.env.VUE_APP_BASE_URL + '/accounts/all-profiles';

class UserService {
    getMe() {
        return axios.get(ME, { headers: authHeader() }).then(response => {
            
            if (response.status === 200) {
                localStorage.setItem('profile', JSON.stringify(response.data));
            }
            return response.data
        })
    }



    getUsers() {
        return axios.get(ALL_PROFILES, { headers: authHeader() });
    }

}

export default new UserService();