import axios from 'axios';
import authHeader from './auth-header'

const COUPON_DETAIL = process.env.VUE_APP_BASE_URL + '/join-coupon';
const REVIEW = process.env.VUE_APP_BASE_URL +'/reviews';

class CouponService {

    createReview(data) {

        var dataToSend = {
            'profile': data['profile'],
            'description': data['description'],
            'rating': data['rating'],
            'product': data['product']
        }
        console.log(dataToSend)
        console.log('New review')
        var resp = axios.post(REVIEW, dataToSend, { headers: authHeader() })
        return resp
    }

    deleteReviewREST(data) {
        var review = data['review_id']
        var resp = axios.delete(`${REVIEW}/${review}`, { headers: authHeader() })
        return resp


    }

    joinCoupon(data) {

        var dataToSend = {
            'coupon_id': data['coupon_id'],
            'amount': data['amount']
        }

        var resp = axios.post(COUPON_DETAIL, dataToSend, { headers: authHeader() })
        return resp
    }
}

export default new CouponService();