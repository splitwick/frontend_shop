import axios from 'axios'
import authHeader from '../services/auth-header'
import UserService from '../services/user.service'

const BASE_URL = process.env.VUE_APP_BASE_URL

const state = {
    userData: {}
};

const getters = {
    AllUserDataInfo: state => state.userData,

};

const actions = {
    async fetchUserData({ commit }) {

        const response = await axios.get(
            BASE_URL+ '/accounts/my-data', { headers: authHeader() }
        );
        commit('setUserData', response.data[0])
    },

    async watchCoupon({ commit }, coupon_id) {
        var param = {
            'coupon_id': coupon_id
        }
        const response = await axios.post(BASE_URL+'/watch-coupon', param, { headers: authHeader() });
        commit('newWatchCoupon', response.data)

    },
    async UpdateProfile({ commit }, data) {

        const response = await axios.post(BASE_URL+'/accounts/update-me', data, { headers: authHeader() });

        if (response.data.msg === 'Success') {
            commit('UpdateProfileMutation', response.data.user);

            UserService.getMe().then(profile => {
                commit('auth/profileSuccess', profile, { root: true })
                return Promise.resolve(profile);
            })
            return response
        }

    }
};

const mutations = {
    setUserData: (state, userData) => (state.userData = userData),
    newWatchCoupon(state, watchCouponData) {
        if (watchCouponData.msg === 'Updated') {
            console.log(state.userData.watches)
            var objIndex = state.userData.watches.findIndex((obj => obj.id == watchCouponData.watchObject.id));
            state.userData.watches[objIndex].watching = watchCouponData.watchObject.watching
        }
        else if (watchCouponData.msg === 'Created') {
            state.userData.watches.unshift(watchCouponData.watchObject)
        }

    },
    UpdateProfileMutation(state, ProfileUpdate) {

        state.userData = ProfileUpdate

    }
};

export default {
    state,
    getters,
    actions,
    mutations
};