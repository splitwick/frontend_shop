import CouponService from '../services/coupon.service'

export const coupon = {
    namespaced: true,
    actions: {

        review({ commit }, data) {
            return CouponService.createReview(data).then(
                response => {
                    return Promise.resolve(response.data);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },

        deleteReview({ commit}, data) {
            return CouponService.deleteReviewREST(data).then(
                response => {
                    return Promise.resolve(response.data);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },

        

        join({ commit }, data) {
            return CouponService.joinCoupon(data).then(
                response => {
                    // commit('registerSuccess');
                    return Promise.resolve(response.data);
                },
                error => {
                    // commit('registerFailure');
                    return Promise.reject(error);
                }
            );
        }
    },
    
}