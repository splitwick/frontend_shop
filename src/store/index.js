  
import Vue from 'vue';
import Vuex from 'vuex';

import { auth } from './auth.module';
import { coupon } from './coupon.module';
import userData  from './userData.module';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    coupon,
    userData
  }
});