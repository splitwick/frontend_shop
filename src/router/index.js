import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    props: true,
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
    props: true,
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue'),
    props: true,
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('../views/Register.vue'),
    props: true,

  },
  {
    path: '/profile/:option',
    name: 'Profile',
    component: () => import('../views/Profile.vue'),
    props: true,

  },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/coupons/:category',
    name: 'Coupons',
    props:true,
    component: () => import('../views/CouponsView.vue')
  },
  {
    path: '/coupon/:couponID',
    name: 'CouponDetailView',
    component: () => import(/* webpackChunkName: "about" */ '../views/CouponDetailView.vue'),
    props: true,
  },
  {
    path: '/detail/:productID',
    name: 'Detail',
    component: () => import(/* webpackChunkName: "about" */ '../views/Detail.vue'),
    props: true,
  },
  
  {
    path: '/products',
    name: 'Products',
    component: () => import(/* webpackChunkName: "about" */ '../views/ProductsView.vue'),
    props: true,
  },

]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router

router.beforeEach((to, from, next) => {
  const publicPages = ['/login', '/register', '/home', '/'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('user');

  // trying to access a restricted page + not logged in
  // redirect to login page
  if (authRequired && !loggedIn) {
    next('/login');
  } else {
    next();
  }
});