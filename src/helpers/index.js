const WAITING_TO_OPEN = 'WAITING_TO_OPEN'  // before opening we may stack some nice coupons
const OPEN = 'OPEN'  // coupon is open, people can join
const CLOSED_SUCCESS = 'CLOSED_SUCCESS'  // people bought product
const CLOSED_NOT_SUCCESS = 'CLOSED_NOT_SUCCESS'  // time expired

export const COUPON_STATUS = {
    WAITING_TO_OPEN: WAITING_TO_OPEN,
    OPEN: OPEN,
    CLOSED_SUCCESS: CLOSED_SUCCESS,
    CLOSED_NOT_SUCCESS: CLOSED_NOT_SUCCESS
}
